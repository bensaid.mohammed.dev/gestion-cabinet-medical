// Call the dataTables jQuery plugin
$(document).ready(function() {
  //$('#dataTable').DataTable();

  var table = $('#dataTable').DataTable();

  $('#dataTable tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
          $(this).removeClass('selected');
          
      }
      else {
          table.$('tr.selected').removeClass('selected');
          $(this).addClass('selected');
      }
  } );

  $('#button').click( function () {
      table.row('.selected').remove().draw( false );
  } );
});
