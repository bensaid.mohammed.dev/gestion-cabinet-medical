<?php
$id = "-1";
$nom = "";
$prenom = "";
$cin = "";
$date_naissance=0;
$tel = "";
$genre = "";

if (isset($_GET["id"])) {
    $id = $_GET["id"];
}

if (isset($_GET["nom"])) {
    $nom = $_GET["nom"];
}
if (isset($_GET["prenom"])) {
    $prenom = $_GET["prenom"];
}
if (isset($_GET["cin"])) {
    $cin = $_GET["cin"];
}
if (isset($_GET["date_naissance"])) {
    $date_naissance = $_GET["date_naissance"];
}
if (isset($_GET["tel"])) {
    $tel = $_GET["tel"];
}
if (isset($_GET["genre"])) {
    $genre = $_GET["genre"];
}
?>
<div class="card shadow-lg border-0 rounded-lg mt-5">
    <div class="card-body">
        <form method="POST" action="controller/patient/controller_patient_post.php">
        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
            <div class="form-row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="small mb-1" for="Nom">Nom</label>
                        <input class="form-control py-4" id="Nom" type="text" placeholder="Entrer le nom" value="<?php echo $nom ?>" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="small mb-1" for="Prenom">Prenom</label>
                        <input class="form-control py-4" id="Prenom" type="text" placeholder="Entrer le prenom" value="<?php echo $prenom ?>" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="small mb-1" for="Cin">CIN</label>
                        <input class="form-control py-4" id="Cin" type="text" placeholder="Entrer la CIN" value="<?php echo $cin ?>" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="small mb-1" for="Tel">Tel</label>
                        <input class="form-control py-4" id="Tel" type="text" placeholder="Entrer le telephone" value="<?php echo $tel ?>" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="small mb-1" for="DateNaissance">Date de naissance</label>
                        <input class="form-control py-4" id="DateNaissance" type="text" placeholder="Enter la Date de naissance" value="<?php echo $date_naissance ?>" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="small mb-1" for="Genre">le genre</label>
                        <input class="form-control py-4" id="Genre" type="text" placeholder="entrer le genre" value="<?php echo $genre ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group mt-4 mb-0">
                <input class="btn btn-primary btn-block" type="submit">
            </div>
        </form>
    </div>
    <div class="card-footer text-center">
        <div class="small"><a href="index.php?contenus=patient&action=liste">Annuler</a></div>
    </div>
</div>