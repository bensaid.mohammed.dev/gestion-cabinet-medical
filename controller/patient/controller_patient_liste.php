<?php

$list = $dao_patient->afficher_patients();

?>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>CIN</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Date de Naissance</th>
            <th>TEL</th>
            <th>Genre</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>CIN</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Date de Naissance</th>
            <th>TEL</th>
            <th>Genre</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>

        <?php

        foreach ($list as $key => $value) {
        ?>
            <tr>
                <td> <?php echo $value['cin'] ?></td>
                <td> <?php echo $value['nom'] ?></td>
                <td> <?php echo $value['prenom'] ?></td>
                <td> <?php echo $value['date_naissance'] ?></td>
                <td> <?php echo $value['tel'] ?></td>
                <td> <?php echo $value['genre'] ?></td>

                <td>
                    <a id="btnModifier" class="btn btn-primary" href="index.php?contenus=patient&action=modifier&id=<?php echo $value['id']; ?>&nom=<?php echo $value['nom']; ?>&prenom=<?php echo $value['prenom']; ?>&cin=<?php echo $value['cin']; ?>&date_naissance=<?php echo $value['date_naissance']; ?>&tel=<?php echo $value['tel']; ?>&genre=<?php echo $value['genre'] ?>">modifier</a>
                    <a id="btnSupprimerFromTable" class="btn btn-danger" href="index.php?contenus=patient&action=supprimer&id=<?php echo $value['id']; ?>">supprimer</a>
                </td>
            </tr>
        <?php
        } ?>


    </tbody>
</table>