<h1 class="mt-4">Patient</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
    <li class="breadcrumb-item active">Patient</li>
    <li class="breadcrumb-item active">
        <?php

        //in this section we include all recuired fils one time and we use it in all patient controllers
        include_once ("dao/dao_patient.php");
        include_once ("model/patient.php");

        $dao_patient = new Dao_Patient();
        
        $action; $id;

        if (isset($_GET["action"])) {
            $action = $_GET["action"];
            
            echo($action);
        }else{
            $action = "";
        }
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            echo(" id = ". $id);
        }else{
            $id= "";
        }
        
        ?>
    </li>
</ol>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        <?php echo $action; ?> patient
    </div>
    <div class="card-body">
        <div class="table-responsive">

            <?php
            switch ($action){
                case  'liste' :
                    //action instruction
                    include('controller_patient_liste.php');  
                break;
                case  'supprimer' :
                    //delete instruction
                    $dao_patient->supprimer_patient($id);
                    include('controller_patient_liste.php');  
                break;
                case  'modifier' :
                    // update instruction
                    include('controller_patient_form.php');  
                break;
                case  'ajouter' :
                    // add instruction
                    include('controller_patient_form.php');  
                break;
                default:
                    //reaload with liste instruction 
                break;
            }
            ?>
        </div>
    </div>
</div>