<main>
    <div class="container-fluid">
        <?php
        if (isset($_GET["contenus"])) {
            $contenus = $_GET["contenus"];
        }else{
            $contenus = "";
        }
        
        switch ($contenus){
            case  'dashboard' :
                include('view/dashboard.php');   
            break;
            case  'charts' :
                include('view/charts.php');   
            break;
            case  'tables' :
                include('view/tables.php');   
            break;
            case  'patient' :
                include('controller/patient/controller_patient.php');   
            break;
            case  'rdv' :
                include('view/rdv.php');   
            break;
            default:
                include('view/dashboard.php'); 
            break;
        }
        ?>
    </div>
</main>