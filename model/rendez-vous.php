<?php 
class RendezVous{
     private $id;
     private $date;
     private $idPatient;
     private $idSecretaire;

     public function __construct($id, $date, $idPatient, $idSecretaire) {
          $this->id = $id;
          $this->date = $date;
          $this->idPatient = $idPatient;
          $this->idSecretaire = $idSecretaire;
     }


    
     public function getId() {
          return $this->id;
     }

     
     public function setId($id) {
          $this->id = $id;
     }

    
     public function getDate() {
          return $this->date;
     }

     
     public function setDate($date) {
          $this->date = $date;
     }

     
     public function getIdPatient() {
          return $this->idPatient;
     }

    
     public function setIdPatient($idPatient) {
          $this->idPatient = $idPatient;
     }

    
     public function getIdSecretaire() {
          return $this->idSecretaire;
     }

    
   
     public function setIdSecretaire($idSecretaire) {
          $this->idSecretaire = $idSecretaire;
     }


}

?>