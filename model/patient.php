<?php
class Patient {
    private int $id;
    private string $cin;
    private string $nom;
    private string $prenom;
    private string $date_naissance;
    private string $tel;
    private bool $genre;

    public function __construct(){ 
    }

    public function setId(int $id){
        $this->id=$id;
    }

    public function getId(){
        return $this->id;
    }

    public function setCin(string $cin){
        $this->cin=$cin;
    }
    public function setNom(string $nom){
        $this->nom=$nom;
    }
    public function setPrenom(string $prenom){
        $this->prenom=$prenom;
    }
    public function setDateNaissance(string $date_naissance){
        $this->date_naissance=$date_naissance;
    }
    public function setTel(string $tel){
        $this->tel=$tel;
    }
    public function setGenre(bool $genre){
        $this->genre=$genre;
    }
    public function getCin(){
        return $this->cin;
    }
    public function getNom(){
        return $this->nom;
    }
    public function getPrenom(){
        return $this->prenom;
    }
    public function getDateNaissance(){
        return $this->date_naissance;
    }
    public function getTel(){
        return $this->tel;
    }
    public function getGenre(){
        return $this->genre;
    }

}
?>