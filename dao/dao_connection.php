<?php 
class Dao_connection{
     private $servername = "localhost";// modifiable
     private $username = "root";// à definir
     private $password = "";// à definir
     private $dbname = "cabinet_medical";// modifiable  
     private $con = null;                                                                           
    



//Connection à la base de données
     public function __construct() {
          try {
               if (empty($this->con)) {
               $this->con = new PDO('mysql:host='.$this->servername.';dbname='.$this->dbname, $this->username, $this->password);
               }

          } catch (PDOException $e) {
               print "Erreur !: " . $e->getMessage() . "<br/>";
               die();
          }
     }

     public function getCon(){
          return $this->con;
      }

}
